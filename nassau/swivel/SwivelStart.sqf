/*	
	Author:
		Madin
	
	Description:
		init scripts on mission start

*/
_handler1 = execVM "nassau\swivel\swivelInit.sqf";
_handler2 = execVM "nassau\swivel\swivelShot.sqf";
_handler3 = execVM "nassau\swivel\swivelAi.sqf";
_handler4 = execVM "nassau\swivel\swivelFindGun.sqf";
execVM "nassau\swivel\spawnShip.sqf";
execVM "nassau\swivel\aresModules.sqf";
waitUntil {scriptDone _handler1}; 
waitUntil {scriptDone _handler2}; 
waitUntil {scriptDone _handler3};
waitUntil {scriptDone _handler4};  
if (isServer) then {
	sleep 2;
	private _size = worldSize;
	private _allSwivels = [_size/2,_size/2,0] nearEntities ["StaticWeapon", _size*2];
	{
		private _cannon = _x;
		private _typeCannon = typeOf _x;
		[_x] call AF_fnc_swivelFindGun;
	}forEach _allSwivels;
};