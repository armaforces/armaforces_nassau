/*
	AF_fnc_swivelFindGun
	
	Author:
		Madin
	
	Description:
		Init "AI" for cannon on mission start

	Arguments:
		0: cannon <OBJECT>

	Return Value:
		None
*/
AF_fnc_swivelFindGun = {
params ["_cannon"];
	private _typeCannon = typeOf _cannon;
	{
		if (_x == _typeCannon) then
		{
			if (crew _cannon isEqualTo []) then
			{
				{[_cannon] call AF_fnc_swivelInit} call CBA_fnc_directCall;
			}else
			{
				[{[_this] call AF_fnc_swivelAi}, _cannon, 1] call CBA_fnc_waitAndExecute;
			};
		};
	}forEach ["1715_eng_1pdrswivel","1715_swivel_french","1715_swivel_pirates"];
};