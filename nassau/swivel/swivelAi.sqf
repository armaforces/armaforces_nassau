/*
	AF_fnc_swivelAi
	
	Author:
		Madin
	
	Description:
		"AI" for cannons. When allive AI is present near cannon, "AI" will seek for target.

	Arguments:
		0: cannon <OBJECT>

	Return Value:
		None
*/
AF_fnc_swivelAi = {
params ["_cannon"];
if (!alive _cannon) exitWith {};
//systemChat "loop";
private _list = _cannon nearEntities ["Man", 3];
private _find = _list findIf {!isPlayer _x && {isNull objectParent _x}};
if (_find != -1) then
{
	//systemChat "znaleziono AI";
	private _listShips = _cannon nearEntities ["Ship", 800];
	if (_listShips isEqualTo [])exitWith {
		//systemChat "nie znaleziono celu";
		[{[_this] call AF_fnc_swivelAi}, _cannon, 3+random 2] call CBA_fnc_waitAndExecute;
	};
	private _sortShips = [];
	{
		_sortShips pushBack [_x distance _cannon,_x];
	}forEach _listShips;
	private _target = false;
	while {!(_sortShips isEqualTo [])}do
	{
		//systemChat "szukam celu";
		private _shipElement = _sortShips deleteAt 0;
		private _ship = _shipElement select 1;
		private _listTarget = _ship nearEntities ["Man", 30];
		_findTarget = _listTarget findIf {isPlayer _x};
		if (_findTarget != -1) exitWith {
			_target = true;
			[{_this call AF_fnc_swivelShot}, [_cannon,_ship], random [0,1,2]] call CBA_fnc_waitAndExecute;
		};
	};
	//systemChat "koniec loop";
	if !(_target) then
	{
		[{[_this] call AF_fnc_swivelAi}, _cannon, 3+random 2] call CBA_fnc_waitAndExecute;
	};
}else
{
	[{[_this] call AF_fnc_swivelAi}, _cannon, 3+random 2] call CBA_fnc_waitAndExecute;
};
};