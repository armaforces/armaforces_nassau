/*
	AF_fnc_swivelShot
	
	Author:
		Madin
	
	Description:
		Init shoot swivel with aiming error.

	Arguments:
		0: cannon <OBJECT>
		1: target <OBJECT>

	Return Value:
		None
*/
AF_fnc_swivelShot = {
params ["_cannon","_target"];
private _velocity = velocity _target;
private _v1 = (_velocity select 0) * 2;
private _v2 = (_velocity select 1) * 2;
private _v3 = (_velocity select 2) * 2;
private _targetPos = (getPosASL _target) vectorAdd [_v1,_v2,_v3];

private _ang = _cannon getRelDir _targetPos;
private _cannonH = (getPosASL _cannon) select 2;
private _targetH = (getPosASL _target) select 2;
private _distance = (_cannon distance2D _targetPos) - _cannonH;
private _angle = (-(_cannonH - _targetH - 2) atan2 _distance)/30;
private _elev = 0;
if (_distance > 100) then
{
	private _magicAngle = (_distance^1.25)/20000;
	private _randomPower = 0.01 + _distance/5000;
	private _pitch = (_cannon call BIS_fnc_getPitchBank) select 0;
	_elev = _magicAngle + (-_pitch /30) + _angle + ((random _randomPower) - (_randomPower/2));
	_elev min 1;
	_elev max -1;
};
_cannon animateSource ["maingunSource", _elev, 2];

private _fire = false;
private _gunDir = 0;
if (_ang <= 70) then
{
	_gunDir = -_ang / 70;
	_gunDir = (_gunDir - 0.08 + random 0.16) min 1;
	_cannon animateSource ["mainturretSource", _gunDir, 2];
	_fire = true;
}else
{
	if (_ang >= 290) then
	{
		private _newAng = _ang - 360;
		_gunDir = -_newAng / 70;
		_gunDir = (_gunDir - 0.08 + random 0.16) max -1;
		_cannon animateSource ["mainturretSource", _gunDir, 2];
		_fire = true;
	};
};
if (_fire) then
{
	[
		{
			(((_this select 0) animationSourcePhase "mainturretSource") isEqualTo (_this select 1) &&
			((_this select 0) animationSourcePhase "maingunSource") isEqualTo (_this select 2))
		},
		{
			private _cannon = (_this select 0);
			[_cannon, currentWeapon _cannon] call BIS_fnc_fire;
			_cannon addMagazine "1715_6pound_powder_shot";
		},
		[_cannon,_gunDir,_elev],
		3,
		{}
	] call CBA_fnc_waitUntilAndExecute;
	[{[_this] call AF_fnc_swivelAi}, _cannon, 15 + random 1] call CBA_fnc_waitAndExecute;
}else
{
	[{[_this] call AF_fnc_swivelAi}, _cannon, 3+random 2] call CBA_fnc_waitAndExecute;
};
};