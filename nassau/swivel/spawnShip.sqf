/*
	AF_fnc_spawnShip
	
	Author:
		Madin
	
	Description:
		Spawn ship with crew

	Arguments:
		0: position ASL <POS>
		1: index of ship = 0 for french,1 and 2 for british, 3 for pirate  <NUMBER>

	Return Value:
		Spawned ship <OBJECT>
*/
AF_fnc_spawnShip = {
	params ["_pos",["_index",0]];
	private _arr = [
		[
			"1715_sloop_albas",
			"1715_Fren_Sailor_",
			5,
			"1715_Fren_Officer",
			west
		],
		[
			"1715_sloop_antelope",
			"1715_Eng_Sailor_",
			5,
			"1715_justa_3d_a_navblu",
			east
		],
		[
			"1715_sloop_adventure",
			"1715_Eng_Sailor_",
			5,
			"1715_justa_3c_a_navblu",
			east
		],
		[
			"1715_sloop_royaljames",
			"1715_Pir_Af_",
			10,
			"1715_Pir_Af_M_1",
			resistance
		]
	];
	private _element = _arr select _index;
	_element params ["_shipType","_sailor","_typesSailor","_captain","_side"];
	private _ship = createVehicle [_shipType, _pos, [], 0, "CAN_COLLIDE"];
	private _grp = createGroup _side;
	private _unit = _grp createUnit [_captain, [0,0,0], [], 0, "CAN_COLLIDE"];
	_unit moveInAny _ship;
	private _shipManPos = [[[2.19,1.38,-11.9],70],[[2.64,-2.03,-12.17],90],[[2.59,-6.68,-12.25],90],[[2.45,-11.4,-11.8],90],[[2.09,-14,-11.55],90]];
	private _sailors = [];
	{
		_x params ["_pos","_dir"];
		private _agent = createAgent [_sailor + str floor ((random (_typesSailor)) + 1), [0,0,0], [], 0, "CAN_COLLIDE"];
		_agent attachTo [_ship, _pos]; 
		_agent SetDir _dir;
		_sailors pushBack _agent;
		//{_x addCuratorEditableObjects [[_agent],true ]}forEach allCurators;
		//[_agent,"AidlPercMstpSnonWnonDnon_G0"+ str floor (random 7)] remoteExecCall ["switchMove",0,true];
	}forEach _shipManPos;
	{
		_x params ["_pos","_dir"];
		private _agent = createAgent [_sailor + str floor ((random (_typesSailor)) + 1), [0,0,0], [], 0, "CAN_COLLIDE"];
		private _pos1 = _pos select 0;
		_pos set [0,-_pos1];
		_agent attachTo [_ship, _pos]; 
		_agent SetDir -_dir;
		_sailors pushBack _agent;
		//{_x addCuratorEditableObjects [[_agent],true ]}forEach allCurators;
		//[_agent,"AidlPercMstpSnonWnonDnon_G0"+ str floor (random 7)] remoteExecCall ["switchMove",0,true];
	}forEach _shipManPos;
	_ship setVariable ["AF_Sailors",_sailors];
	_ship addEventHandler ["Deleted", {
		params ["_entity"];
		private _sailors = _entity getVariable ["AF_Sailors",[]];
		{deleteVehicle _x}forEach _sailors;
	}];
	[
		{
			[_this] call BW_sail_fnc_addCannons;
			[
			{
				{
					[_x] call AF_fnc_swivelFindGun;
				}forEach (attachedObjects _this);
				[_this,0] call BW_sail_fnc_setAllSails;
			}, 
			_this,
			1
			] call CBA_fnc_waitAndExecute;
		}, 
		_ship,
		1
	] call CBA_fnc_waitAndExecute;
	{_x addCuratorEditableObjects [[_ship],true ]}forEach allCurators;
	_ship
};