/*	
	Author:
		Madin

	Description:
		Adds spawn ships to zeus

*/
if !(isClass(configFile >> "cfgPatches" >> "achilles_data_f_achilles")) exitWith {};
["Spawn ship", "French Albas", 
{
	params [["_position", [0,0,0], [[]], 3], ["_objectUnderCursor", objNull, [objNull]]];
	_position set [2,0];
	[_position,0] remoteExec ["AF_fnc_spawnShip",2];
}] call Ares_fnc_RegisterCustomModule;
["Spawn ship", "Britan Adventure", 
{
	params [["_position", [0,0,0], [[]], 3], ["_objectUnderCursor", objNull, [objNull]]];
	_position set [2,0];
	[_position,1] remoteExec ["AF_fnc_spawnShip",2];
}] call Ares_fnc_RegisterCustomModule;
["Spawn ship", "Britan Antelope", 
{
	params [["_position", [0,0,0], [[]], 3], ["_objectUnderCursor", objNull, [objNull]]];
	_position set [2,0];
	[_position,2] remoteExec ["AF_fnc_spawnShip",2];
}] call Ares_fnc_RegisterCustomModule;
["Spawn ship", "Pirate Royal James", 
{
	params [["_position", [0,0,0], [[]], 3], ["_objectUnderCursor", objNull, [objNull]]];
	_position set [2,0];
	[_position,2] remoteExec ["AF_fnc_spawnShip",2];
}] call Ares_fnc_RegisterCustomModule;
