/*
	AF_fnc_swivelInit
	
	Author:
		Madin
	
	Description:
		Put UAV unit in empty cannon

	Arguments:
		0: cannon <OBJECT>

	Return Value:
		None
*/
AF_fnc_swivelInit = {
params ["_cannon"];
private _group = createGroup civilian;   
private _unit = _group createUnit ["O_UAV_AI", position _cannon, [], 0, "CAN_COLLIDE"];   
_unit setCaptive true;   
_unit disableAI "ALL";   
_unit moveInGunner _cannon;
[{[_this] call AF_fnc_swivelAi}, _cannon, 1] call CBA_fnc_waitAndExecute;
};