[
	"Nassau Madin Fix'it",
	"AF_nassau_main",
	"Main actions",
	{
		call BW_sail_fnc_OnActionKey;
		BW_actionKey = true;
		false;
	},
	{
		BW_actionKey = false;
		if(BW_dragging)then{
			[BW_draggingData#0,2] remoteExecCall ["setOwner",2];
			["dragAction",{}] call BW_sail_fnc_stackOnEachFrame;
			BW_draggingData = [];
			BW_dragging = false;
		};
	},
	[33, [false, false, false]]
] call cba_fnc_addKeybind;
["Nassau Madin Fix'it", "AF_nassau_main"] call CBA_fnc_getKeybind;
[
	"Nassau Madin Fix'it",
	"AF_nassau_steerLeft",
	"Steer Left",
	{
		if(!isNull BW_cShip && {player distance (BW_cShip modelToWorld (BW_cShip selectionPosition "pos_helm"))<2 && "rudder" in (animationNames BW_anker)})then{
			_anim = (BW_cShip animationphase "wheel") - .1;
			if(_anim<-1)then{
				_anim = -1;
			};
			BW_cShip animate ["wheel", _anim];
			BW_cShip animate ["drumrope", _anim];
			BW_cShip animate ["rudder", _anim];
			true;
		};
	},
	{
	},
	[30, [false, true, false]],
	true
] call cba_fnc_addKeybind;
[
	"Nassau Madin Fix'it",
	"AF_nassau_steerRight",
	"Steer Right",
	{
		if(!isNull BW_cShip && {player distance (BW_cShip modelToWorld (BW_cShip selectionPosition "pos_helm"))<2 && "rudder" in (animationNames BW_anker)})then{
			_anim = (BW_cShip animationphase "wheel") + .1;
			if(_anim>1)then{
				_anim = 1;
			};
			BW_cShip animate ["wheel", _anim];
            BW_cShip animate ["drumrope", _anim];
            BW_cShip animate ["rudder", _anim];
            true;
		};
	},
	{
	},
	[32, [false, true, false]],
	true
] call cba_fnc_addKeybind;