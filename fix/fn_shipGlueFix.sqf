player addEventHandler ["InventoryOpened", {
	private _pos = getposASL player;
	private _intersects = lineIntersectsObjs [_pos, _pos vectorAdd [0,0,-50], objNull, player, false, 2];
	private _object = _intersects select 0;
	if (_object isKindOf "ship") then
	{
		[player, _object] call BIS_fnc_attachToRelative;
	};
}];
player addEventHandler ["InventoryClosed", {
	private _object = attachedTo player;
	if (_object isKindOf "ship") then {
		detach player;
	};
}];